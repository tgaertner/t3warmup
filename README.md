# WARMUP

A bash script to crawl a website based on a given sitemap.xml to warm up caches.

The auto-mode just works with TYPO3 V9 and above and reads the URLs from SiteConfigurations - this just works if you run the script on the Production-Server directly.

Other then that is should work with any Website providing a valid sitemap.xml under your-domain.tld/sitemap.xml.


## Usage

./warmup.sh mode|domain depth

mode|domain can be "auto" or a specific Domain to crawl.

depth specifies how many levels (nested page path segments in a speaking url) should be followed.
