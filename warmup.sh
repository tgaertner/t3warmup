#!/usr/bin/env bash

## Usage: ./warmup.sh mode|domain depth
## Example: "./warmup.sh https://mydomain.tld 5"
## Example: "./warmup.sh auto 2" - will grep Domains from local TYPO3's SiteConfiguration

echo "Start: $(date)"

domain=$1
depth=$2

# check param domain/mode - if empty abort
if [ -z "$domain" ]
then
    echo "required parameter domain/mode missing"
    exit 1
fi

# check param depth - if empty set default value
if [ -z "$depth" ]
then
    echo "parameter depth was not given - taking default depth of 4"
    depth=4
fi

# check if mode "auto" was set - if so, read domains form sites - if not assuming single domain was given
if [ $domain = "auto" ]
then
    echo auto mode detected - reading Domains from SiteConfigurations...
    domainsrc="$(vendor/bin/typo3 site:list | egrep -o "http+[^|]+" | xargs)"
    domains=($(echo $domainsrc | tr " " "\n"  | sed 's/\/$//'))
else
    domains=(
        $domain
    )
fi

function get {
    wget -q --no-check-certificate $1/sitemap.xml --no-cache -O - | egrep -o "$1[^<]+" | while read subsite;
    do
        subsite=$(echo $subsite | sed 's/\&amp;/\&/g')
        echo --- Reading Sub: $subsite ---
        wget -q --no-check-certificate $subsite --no-cache -O - | egrep -o "$1[^<]+" | while read line;
        do
            line=$(echo $line | sed 's/\&amp;/\&/g')
            echo
            count=$(($(echo $line | tr -cd '/' | wc -c)-2))
            if [ ! $count -gt $depth ]
            then
                echo $line
                time curl -sL -A 'Cache Warmer' $line > /dev/null 2>&1
                echo
                echo -----------------------------------------------
            else
                echo "scipping $line - depth of $depth exeeded"
                echo
                echo -----------------------------------------------
            fi
        done
        echo --- FINISHED reading sub-sitemap: $subsite: ---
    done
    echo "Finish: $(date)"
}

for domain in "${domains[@]}" ; do
    echo "$domain"
    get ${domain%/}
done
